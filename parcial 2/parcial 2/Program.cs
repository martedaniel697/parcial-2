﻿using System;

namespace parcial_2
{
    class Program
    {
        static void Main(string[] args)
        {
            float total;
            int voto, x;
            string ganador;
            Random votos = new Random();
            float[] v = new float[4];
            v[0] = votos.Next(1, 1000);
            v[1] = votos.Next(1, 1000);
            v[2] = votos.Next(1, 1000);
            v[3] = votos.Next(1, 1000);
            string[] candidatos = new string[4];
            candidatos[0] = "Rikudou Sennin";
            candidatos[1] = "Kaguya Ōtsutsuki";
            candidatos[2] = "Tobirama Senju";
            candidatos[3] = "Hashirama Senju";
            Console.WriteLine("Partidos");
            Console.WriteLine();
            Console.WriteLine("Rutenio Samario (RS) , fundado el 09 de septiembre de 2011");
            Console.WriteLine("Kryptón Osmio(KO), fundado el 03 de diciembre de 1968");
            Console.WriteLine("Tantalio Selenio(TS), fundado el 03 de octubre del 2019");
            Console.WriteLine("Hafnio Seaborgio(HS),fundado el 20 de febrero de 2016");
            Console.WriteLine();

            Console.WriteLine();

            Console.WriteLine("Candidatos a la presidencia");
            Console.WriteLine("(1)Rikudou Sennin-RS");
            Console.WriteLine("(2)Kaguya Ōtsutsuki-KO");
            Console.WriteLine("(3)Tobirama Senju-TS");
            Console.WriteLine("(4)Hashirama Senju-HS");
            Console.WriteLine();
            Console.WriteLine("Elija su candidato");
            voto = int.Parse(Console.ReadLine());

            switch (voto)
            {
                case 1:
                    v[0] = v[0] + 1;
                    break;
                case 2:
                    v[1] = v[1] + 1;
                    break;
                case 3:
                    v[2] = v[2] + 1;
                    break;
                case 4:
                    v[3] = v[3] + 1;
                    break;

            }


            Console.WriteLine("La cantidad de votos de Rikudou Sennin fueron {0}", v[0]);
            Console.WriteLine("La cantidad de votos de Kaguya Ōtsutsuki fueron {0}", v[1]);
            Console.WriteLine("La cantidad de votos de Tobirama Senju {0}", v[2]);
            Console.WriteLine("La cantidad de votos de Hashirama Senju fueron {0}", v[3]);

            Console.WriteLine();
            total = v[0] + v[1] + v[2] + v[3];
            v[0] = (v[0] / total) * 100;
            v[1] = (v[1] / total) * 100;
            v[2] = (v[2] / total) * 100;
            v[3] = (v[3] / total) * 100;

            Console.WriteLine("El porcentaje de los candidatos son:");
            Console.WriteLine(candidatos[0] + " {0}", v[0] + "%");
            Console.WriteLine(candidatos[1] + " {0}", v[1] + "%");
            Console.WriteLine(candidatos[2] + " {0}", v[2] + "%");
            Console.WriteLine(candidatos[3] + " {0}", v[3] + "%");
            Console.WriteLine();
            Console.Write("El nuevo presidente de 'NARNIA' ES :");
            if (v[0] > v[1] && v[0] > v[2] && v[0] > v[3])
            {
                ganador = "Rikudou Sennin";
                Console.WriteLine(ganador);
            }
            if (v[1] > v[0] && v[1] > v[2] && v[1] > v[3])
            {
                ganador = "Kaguya Ōtsutsuki";
                Console.WriteLine(ganador);
            }
            if (v[2] > v[1] && v[2] > v[0] && v[2] > v[3])
            {
                ganador = "Tobirama Senju";
                Console.WriteLine(ganador);
            }
            if (v[3] > v[1] && v[3] > v[2] && v[3] > v[0])
            {
                ganador = "Hashirama Senju";
                Console.WriteLine(ganador);
            }
            Console.ReadKey();


        }
    }
}
